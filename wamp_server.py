import asyncio
import socket

from config import config_g
from color import Color
from logger import logger

import json

from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner

class Component(ApplicationSession):
    """
    bla
    """
    
    @asyncio.coroutine
    def onJoin(self, details):
        # Subscribes
        def on_touch(i):
            config_g._positions = i

        yield from self.subscribe(on_touch, 'bzh.splann3.touch')
        
        #Registers
        
        def anims():
            anims = []
            for anim in self.core.playlist.animations:
                anims.append(str(anim).split("'")[1])
            return anims
        reg = yield from self.register(anims, 'bzh.splann3.anims')
        
        def anims_playlist():
            print(self.core.playlist)
            anims = []
            for anim in self.core.playlist:
                anims.append(str(anim).split("'")[1])
            return anims
        reg = yield from self.register(anims_playlist, 'bzh.splann3.anims.playlist')
        
        # vérifier qu'on ne me passe pas de la merde pour zap et add
        def anims_zap(anim=None, args={}):
            if anim:
                self.core.playlist.add(anim, args, position=0)
            config_g.zap = True
            self.publish('bzh.splann3.change', ('zap', anim, args))
        reg = yield from self.register(anims_zap, 'bzh.splann3.anims.zap')
        
        def anims_playlist_add(anim, args={}):
            self.core.playlist.add(anim, args)
            self.publish('bzh.splann3.change', ('playlist_add', anim, args))
        reg = yield from self.register(anims_playlist_add, 'bzh.splann3.anims.add')
        
        def anim_delai(delai = None):
            if delai:
                try:
                    old_value = config_g.delai
                    config_g.delai = int(delai)
                    self.publish('bzh.splann3.change', {'delai':{'old_value': old_value, 'new_value': config_g.delai}})
                except ValueError:
                    logger.warning("delai: incorrect input")
            return config_g.delai
        reg = yield from self.register(anim_delai, 'bzh.splann3.anim.delai')
        
        def anim_pause(pause = None):
            if pause:
                old_value = config_g.pause
                if pause == "true":
                    config_g.pause = True
                elif pause == "false":
                    config_g.pause = False
                self.publish('bzh.splann3.change', {'pause':{'old_value': old_value, 'new_value': config_g.pause}})
            return config_g.pause
        reg = yield from self.register(anim_pause, 'bzh.splann3.anim.pause')
        
        def anim_stick(stick = None):
            if stick:
                old_value = config_g.stick
                if stick == "true":
                    config_g.stick = True
                elif stick == "false":
                    config_g.stick = False
                self.publish('bzh.splann3.change', {'stick':{'old_value': old_value, 'new_value': config_g.stick}})
            return config_g.stick
        reg = yield from self.register(anim_stick, 'bzh.splann3.anim.stick')
        
        def color():
            color = {
                'hue': Color.hue_global,
                'delta': Color.delta_global,
                'variation': Color.variation_global
            }
            return color
        reg = yield from self.register(color, 'bzh.splann3.color')
        
        def color_hue(hue = None):
            if hue:
                try:
                    old_value = Color.hue_global
                    Color.hue_global = float(hue)%1
                    self.publish('bzh.splann3.change', {'color_hue':{'old_value': old_value, 'new_value': Color.hue_global}})
                except ValueError:
                    logger.warning("Color.hue_global: incorrect input")
            return Color.hue_global
        reg = yield from self.register(color_hue, 'bzh.splann3.color.hue')
        
        def color_delta(delta = None):
            if delta:
                try:
                    old_value = Color.delta_global
                    delta = float(delta)
                    Color.delta_global = min(delta, 0.5)
                    self.publish('bzh.splann3.change', {'color_delta':{'old_value': old_value, 'new_value': Color.delta_global}})
                except ValueError:
                    logger.warning("Color.delta_global: incorrect input")
            return Color.delta_global
        reg = yield from self.register(color_delta, 'bzh.splann3.color.delta')
        
        def color_variation(variation = None):
            if variation:
                try:
                    old_value = Color.variation_global
                    Color.variation_global = int(variation)
                    self.publish('bzh.splann3.change', {'color_variation':{'old_value': old_value, 'new_value': Color.variation_global}})
                except ValueError:
                    logger.warning("Color.variation_global: incorrect input")
            return Color.variation_global
        reg = yield from self.register(color_variation, 'bzh.splann3.color.variation')
        
        def anims_filters_color(color = -1):
            if color in [0, 1, 2]:
                old_value = self.core.playlist.filter_color
                self.core.playlist.filter_color = color
                self.publish('bzh.splann3.change', {'filters_color':{'old_value': old_value, 'new_value': self.core.playlist.filter_color}})
            return self.core.playlist.filter_color
        reg = yield from self.register(anims_filters_color, 'bzh.splann3.anims.filters.color')
        
        def anims_filters_test(test = None):
            if test:
                old_value = self.core.playlist.filter_test
                if test == "true":
                    self.core.playlist.filter_test = True
                elif test == "false":
                    self.core.playlist.filter_test = False
                self.publish('bzh.splann3.change', {'filters_test':{'old_value': old_value, 'new_value': self.core.playlist.filter_test}})
            return self.core.playlist.filter_test
        reg = yield from self.register(anims_filters_test, 'bzh.splann3.anims.filters.test')
        
        def anims_filters_audio(audio = None):
            if audio:
                old_value = self.core.playlist.filter_audio
                if audio == "true":
                    self.core.playlist.filter_audio = True
                elif audio == "false":
                    self.core.playlist.filter_audio = False
                self.publish('bzh.splann3.change', {'filters_audio':{'old_value': old_value, 'new_value': self.core.playlist.filter_audio}})
            return self.core.playlist.filter_audio
        reg = yield from self.register(anims_filters_audio, 'bzh.splann3.anims.filters.audio')
        
        def server_serial(serial = None):
            if serial:
                old_value = config_g._serial_ready
                if serial == "true":
                    config_g.active_serial()
                elif serial == "false":
                    config_g.deactive_serial()
                self.publish('bzh.splann3.change', {'server_serial':{'old_value': old_value, 'new_value': config_g._serial_ready}})
            return config_g._serial_ready
        reg = yield from self.register(server_serial, 'bzh.splann3.server.serial')
        
        def server_infos():
            infos = {
                "delai" : config_g.delai,
                "hue_global" : Color.hue_global,
                "delta_global" : Color.delta_global,
                "variation_global" : Color.variation_global
            }
            filters = {
                "test" : self.core.playlist._test,
                "audio" : self.core.playlist._audio,
                "color" : self.core.playlist._couleur
            }
            server = {
                "pause" : config_g.pause,
                "stick" : config_g.stick,
                "serial" : config_g._serial_ready,
                "cases" : config_g.cases
            }
            anim = {
                "anim" : str(self.core.playlist.current[0]).split("'")[1],
                "args" : str(self.core.playlist.current[1])
            }
            return {"filters":filters, "server":server, "infos":infos, "anim":anim}
        reg = yield from self.register(server_infos, 'bzh.splann3.server.infos')
        
        def server_end(end):
            if end == "true":
                old_value = config_g.end
                logger.info("Demande d'arret reçue. Terminé, bonsoir.")
                config_g.end = True
                self.publish('bzh.splann3.change', {'server_end':{'old_value': old_value, 'new_value': config_g.end}})
            return config_g.end
        reg = yield from self.register(server_end, 'bzh.splann3.server.end.yes.i.am.sure')
        
        # Publication des matrices et infos associées
        while not config_g.end:
            self.publish('bzh.splann3.anim.matrix', self.core.matrix)
            self.publish('bzh.splann3.anim.infos', self.core.matrix.infos)
            self.publish('bzh.splann3.draw', config_g.draw)
            yield from asyncio.sleep(self.delai_min_max(config_g.delai))

    def delai_min_max(self, delai):
        return max(min(500, int(delai)), 100)/1000

class WAMP_Server():
    
    def run(self, core):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        runner = ApplicationRunner(url="ws://"+ config_g.server_ip +":8080/ws",
                                        realm="realm1")
        Component.core = core
        runner.run(Component)
