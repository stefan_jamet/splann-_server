import socket
import serial
from logger import logger

class Config():
    
    def __init__(self):
        self.cases = 5
        self.delai = 200
        
        # On récupère notre adresse IP sur le réseau local
        # C'est une astuce qui demande de se connecter et donc
        #  à une IP externe, on a besoin d'une connexion internet.
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        # On stocke l'adresse IP locale dans un conteneur
        # qui sera accessible partout ailleur.
        self.server_ip = s.getsockname()[0]
        s.close()
        
        self.port = "/dev/ttyACM0"
        self.baudrate = 115200
        
        self.end = False
        self.pause = False
        self.zap = False
        self.stick = False
        
        self._serial_ready = False
        self._serial = None
        
        self.draw = False
        self._positions = []
    
    def active_serial(self):
        if not self._serial_ready:
            try:
                self._serial = serial.Serial(port=self.port, baudrate=self.baudrate, bytesize=8, parity='N', stopbits=1, timeout=1)
            except:
                logger.error("bordel de merde !!!!, serial ne marcho po")
            else:
                self._serial.close()
                self._serial.open()
                retour = "."
                self._serial.write(retour.encode('ascii'))
                time.sleep(0.5)
                self._serial_ready = True
                print("activation !!!!!??")
    
    def deactive_serial(self):
        self._serial_ready = False
        self._serial.close()
    
    @property
    def cases(self):
        return self._cases
    
    @cases.setter
    def cases(self, val):
        self._cases = val
        self.cases_r = range(0, val)
    
config_g = Config()