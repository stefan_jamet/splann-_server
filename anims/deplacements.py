import time
import random
from math import *

from .base import Anims, MIX, RGB
from color import Color
from config import config_g


class Deplacements(Anims):
    def mM(self, x):
        return 0 if x < 0 else 1 if x > 1 else x

class Pong(Deplacements):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 20)
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(150,200))
        definition = 100
        positionX = random.randint(0, int((config_g.cases-1)*definition))
        positionY = random.randint(0, int((config_g.cases-1)*definition))
        deplacementX = random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])
        deplacementY = random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])
        couleurPoint= Color.new_color()
        rayonMax = config_g.cases*random.randint(int(definition/3),int(definition/2.5))
        rayon_coeff = (200*definition) / rayonMax
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            positionX += deplacementX
            positionY += deplacementY
            if positionX < 0 or positionX > (config_g.cases-1)*definition:
                deplacementX = -deplacementX
            if positionY < 0 or positionY > (config_g.cases-1)*definition:
                deplacementY = -deplacementY
            for x in config_g.cases_r:
                for y in config_g.cases_r:
                    Distance = sqrt((positionX-(x*definition))**2 + (positionY-(y*definition))**2) * rayon_coeff
                    for color in Color.colors:
                        base_matrix[x][y][color] -= fade_val
                        var_t = base_matrix[x][y][color] + couleurPoint[color]- Distance/definition
                        base_matrix[x][y][color] = 0 if var_t < 0 else 255 if var_t > 255 else var_t

            self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
            couleurPoint = Color.varie(couleurPoint)
        return base_matrix

class Pong_RGB(Deplacements):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 20)
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(150,200))
        definition = 100
        positionX = [random.randint(0, int((config_g.cases-1)*definition)),random.randint(0, int((config_g.cases-1)*definition)),random.randint(0, int((config_g.cases-1)*definition))]
        positionY = [random.randint(0, int((config_g.cases-1)*definition)),random.randint(0, int((config_g.cases-1)*definition)),random.randint(0, int((config_g.cases-1)*definition))]
        deplacementX = [random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])]
        deplacementY = [random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])]
        couleurPoint= Color.new_color()
        rayonMax = [config_g.cases*random.randint(int(definition/3),int(definition/2.5)),config_g.cases*random.randint(int(definition/3),int(definition/2.5)),config_g.cases*random.randint(int(definition/3),int(definition/2.5))]
        rayon_coeff = [(200*definition) / rayonMax[0], (200*definition) / rayonMax[1], (200*definition) / rayonMax[2]]
        Distance = [0,0,0]
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            for c in Color.colors:
                positionX[c] += deplacementX[c]
                positionY[c] += deplacementY[c]
                if positionX[c] < 0 or positionX[c] > (config_g.cases-1)*definition:
                    deplacementX[c] = -deplacementX[c]
                if positionY[c] < 0 or positionY[c] > (config_g.cases-1)*definition:
                    deplacementY[c] = -deplacementY[c]
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        base_matrix[x][y][c] -= fade_val
                        Distance[c] = sqrt((positionX[c]-(x*definition))**2 + (positionY[c]-(y*definition))**2) * rayon_coeff[c]
                        var_t = base_matrix[x][y][c] + couleurPoint[c]- Distance[c]/definition
                        base_matrix[x][y][c] = 0 if var_t < 0 else 255 if var_t > 255 else var_t

            self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
            couleurPoint = Color.varie(couleurPoint)
        return base_matrix


class Dessine(Deplacements):
    coeff = 0
    def play(self, base_matrix, kwargs={}):
        config_g.draw = True
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 15)
        Frames = 0
        definition = 75
        rayonMax = config_g.cases*definition/(config_g.cases*1.5)
        posXprec = 0
        posYprec = 0
        FramesImmo = 0
        dic_infos = Anims.infos(self, 0, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        while not config_g.end:
            start = time.perf_counter()
            dic_infos['frames_total'] = FramesImmo
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            Frames += 1
            if config_g._positions[0]['x'] == posXprec and config_g._positions[0]['y'] == posYprec:
                FramesImmo += 1
                if FramesImmo == 20:
                    config_g.zap = True
            else:
                FramesImmo = 0
            for x in config_g.cases_r:
                for y in config_g.cases_r:
                    for color in Color.colors:
                        base_matrix[x][y][color] -= fade_val
                    for coordonnees in config_g._positions:
                        ColorCible = Color.new_color(float(coordonnees['hue']))
                        positionX = self.mM(coordonnees['x'])*definition*(config_g.cases-1)
                        positionY = self.mM(coordonnees['y'])*definition*(config_g.cases-1)
                        Distance = sqrt((positionX-(x*definition))**2 + (positionY-(y*definition))**2) * (200*definition) / rayonMax
                        for color in Color.colors:
                            var_t = base_matrix[x][y][color] + ColorCible[color] - Distance/definition
                            base_matrix[x][y][color] = 0 if var_t < 0 else 255 if var_t > 255 else var_t

            posXprec = config_g._positions[0]['x']
            posYprec = config_g._positions[0]['y']
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix