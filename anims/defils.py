import time
import random

from .base import Anims, MIX, RGB
from color import Color
from config import config_g


class Defils(Anims):
    def play_std(self, base_matrix, kwargs={}):
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 10)
        kw_orientation = kwargs['Orientation'] = kwargs.get('Orientation', random.randint(0,3))
        kw_sens = kwargs['Sens'] = kwargs.get('Sens', 0) #0 : sens = orientation, 1: sens = +/- orientation, 2: tous les sens

        Position = []
        CouleurLigne = []
        Sens = []
        for p in config_g.cases_r:
            Position.append(random.randint(-2*config_g.cases,-1))
            CouleurLigne.append(Color.new_color())
            if kw_sens == 0:
                Sens.append(kw_orientation)
            elif kw_sens == 1:
                orientation = (random.randint(0,1)*2 + kw_orientation) % 4
                Sens.append(orientation)
            else:
                Sens.append(random.randint(0,3))
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            base_matrix.fade(kw_estompage)
            for x in config_g.cases_r:
                if Position[x] >=0 and Position[x] < config_g.cases:
                    for c in Color.colors:
                        if Sens[x] == 0:
                            base_matrix[Position[x]][x][c] = base_matrix[Position[x]][x][c]  + CouleurLigne[x][c]
                        elif Sens[x] == 1:
                            base_matrix[x][Position[x]][c] = base_matrix[x][Position[x]][c] + CouleurLigne[x][c]
                        elif Sens[x] == 2:
                            base_matrix[config_g.cases-1-Position[x]][x][c] = base_matrix[config_g.cases-1-Position[x]][x][c] + CouleurLigne[x][c]
                        else:
                            base_matrix[x][config_g.cases-1-Position[x]][c] = base_matrix[x][config_g.cases-1-Position[x]][c] + CouleurLigne[x][c]
                elif Position[x] > config_g.cases:
                    Position[x] = random.randint(-2*config_g.cases,-1)
                    CouleurLigne[x] = Color.new_color()
                    #if kw_sens == 0:
                        #Sens[x] = kw_orientation
                    if kw_sens == 1:
                        orientation = (random.randint(0,1)*2+kw_orientation)%4
                        Sens[x] = orientation
                    elif kw_sens == 2:
                        Sens[x] = random.randint(0,3)
                Position[x] += 1
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix
    def play_rgb(self, base_matrix, kwargs={}):
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 10)
        kw_orientation = kwargs['Orientation'] = kwargs.get('Orientation', random.randint(0,3))
        kw_sens = kwargs['Sens'] = kwargs.get('Sens', 0) #0 : sens = orientation, 1: sens = +/- orientation, 2: tous les sens

        Position = []
        CouleurLigne = []
        Sens = []
        for p in config_g.cases_r:
            Position.append([random.randint(-2*config_g.cases,-1),random.randint(-2*config_g.cases,-1),random.randint(-2*config_g.cases,-1)])
            CouleurLigne.append(Color.new_color())
            if kw_sens == 0:
                Sens.append([kw_orientation,kw_orientation,kw_orientation])
            elif kw_sens == 1:
                orientation = [(random.randint(0,1)*2 + kw_orientation) % 4,(random.randint(0,1)*2 + kw_orientation) % 4,(random.randint(0,1)*2 + kw_orientation) % 4]
                Sens.append(orientation)
            else:
                Sens.append([random.randint(0,3),random.randint(0,3),random.randint(0,3)])
        dic_infos = Anims.infos(self,  kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            base_matrix.fade(kw_estompage)
            for x in config_g.cases_r:
                for color in Color.colors:
                    if Position[x][color] >=0 and Position[x][color] < config_g.cases:
                        if Sens[x][color] == 0:
                            base_matrix[Position[x][color]][x][color] = base_matrix[Position[x][color]][x][color]  + CouleurLigne[x][color]
                        elif Sens[x][color] == 1:
                            base_matrix[x][Position[x][color]][color] = base_matrix[x][Position[x][color]][color] + CouleurLigne[x][color]
                        elif Sens[x][color] == 2:
                            base_matrix[config_g.cases-1-Position[x][color]][x][color] = base_matrix[config_g.cases-1-Position[x][color]][x][color] + CouleurLigne[x][color]
                        else:
                            base_matrix[x][config_g.cases-1-Position[x][color]][color] = base_matrix[x][config_g.cases-1-Position[x][color]][color] + CouleurLigne[x][color]
                    elif Position[x][color] > config_g.cases:
                        Position[x][color] = random.randint(-2*config_g.cases,-1)
                        CouleurTemp = Color.new_color()
                        CouleurLigne[x][color] = CouleurTemp[color]
                        #if kw_sens == 0:
                            #Sens[x][color] = kw_orientation
                        if kw_sens == 1:
                            orientation = (random.randint(0,1)*2+kw_orientation)%4
                            Sens[x][color] = orientation
                        elif kw_sens == 2:
                            Sens[x][color] = random.randint(0,3)
                    Position[x][color] += 1
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix

class Defil(Defils):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Defils()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Defil_RGB(Defils):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Defils()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Defil_Toutes_Orientations(Defils):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Sens'] = 2
        obj = Defils()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Defil_Toutes_Orientations_RGB(Defils):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Sens'] = 2
        obj = Defils()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Defil_One_Orientation(Defils):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Sens'] = 1
        obj = Defils()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Defil_One_Orientation_RGB(Defils):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Sens'] = 1
        obj = Defils()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix