import time
import random
import os

from PIL import Image # installer pillow

from .base import Anims, MIX, RGB
from color import Color
from config import config_g


class Images(Anims):
    # Liste les frames d'un gif animé
    def _iter_frames(self, im):
        try:
            i= 0
            while 1:
                im.seek(i)
                imframe = im.copy()
                if i == 0: 
                    palette = imframe.getpalette()
                else:
                    imframe.putpalette(palette)
                yield imframe
                i += 1
        except EOFError:
            pass

class Texte(Images):
    coeff = 2
    def play(self, base_matrix, kwargs={}):
        chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(config_g.cases) + "/alpha/"
        if os.path.isdir(chemin): # le chemin existe, on peut y aller
            ColorCible = Color.new_color()
            Textes = [
                "DO_IT|",
                "MAKE_IT|",
                "ROCK_IT|",
                "HARDER",
                "BETTER",
                "FASTER",
                "STRONGER",
                "GET_LUCKY",
                "DISCO",
                "DANCE!",
                "TECHNOLOGIC",
                "ONE_MORE_TIME",
                "WE_ARE_YOUR_FRIENDS",
                "ROBOT_ROCK"
            ]
            kw_texte = kwargs['Texte'] = kwargs.get('Texte', Textes[random.randint(0,len(Textes)-1)])
            idx = 0
            dic_infos = Anims.infos(self, len(kw_texte), 0, kwargs)
            for Caractere in kw_texte.upper():
                start = time.perf_counter()
                dic_infos['frame'] = idx
                dic_infos['delai'] = config_g.delai
                idx += 1
                if Caractere == "!":# gestion des caractère spéciaux
                    Caractere = "_ex"
                elif Caractere == "|":
                    Caractere = "_dex"
                imgCar = chemin + Caractere + ".png"
                if os.path.isfile(imgCar):
                    im = Image.open(imgCar)
                else:
                    im = Image.open(chemin + "_.png")
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        R, V, B = im.getpixel((x, y))
                        color_temp = [R, V, B]
                        for c in Color.colors:
                            col_calc = color_temp[c] * ColorCible[c]
                            base_matrix[x][y][c] = 255 if col_calc > 255 else col_calc
                ColorCible = Color.varie(ColorCible)
                self.affiche(base_matrix, dic_infos, config_g.delai*2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix

class Pictos(Images):
    def play(self, base_matrix, kwargs={}):
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(5,10))
        chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(config_g.cases) + "/pictos/"
        if os.path.isdir(chemin): # le chemin existe, on peut y aller
            ColorCible = Color.new_color()
            Pictos  = [file for file in os.listdir(chemin) if file.lower().endswith('png')]
            dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
            for Frames in range(0, kw_frames):
                start = time.perf_counter()
                dic_infos['frame'] = Frames
                dic_infos['delai'] = config_g.delai
                Picto = Pictos[random.randint(0,len(Pictos)-1)]

                imgCar = chemin + Picto
                im = Image.open(imgCar)
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        R, V, B = im.getpixel((x, y))
                        color_temp = [R, V, B]
                        if Picto[-5:-4] == "N":
                            for c in Color.colors:
                                col_calc = color_temp[c] * ColorCible[c]
                                base_matrix[x][y][c] = 255 if col_calc > 255 else col_calc
                        else:
                            base_matrix[x][y] = color_temp
                ColorCible = Color.varie(ColorCible)
                self.affiche(base_matrix, dic_infos, config_g.delai*2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix

class Panos(Images):
    def play(self, base_matrix, kwargs={}):
        chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(config_g.cases) + "/panos/"
        if os.path.isdir(chemin): # le chemin existe, on peut y aller
            ColorCible = Color.new_color()
            Pictos  = [file for file in os.listdir(chemin) if file.lower().endswith('png')]
            Picto = Pictos[random.randint(0,len(Pictos)-1)]
            imgCar = chemin + Picto
            Vitesse = int(Picto[-6:-4])/10
            Orientation = Picto[-8:-7]
            if Orientation == "H":
                Dim = 0
            else:
                Dim = 1
            Sens = Picto[-7:-6]
            if Sens == "N":
                if random.randint(0,1) == 0: Sens = "G"
                else: Sens = "D"
            im = Image.open(imgCar)
            if Sens == "G": Range = range(0,im.size[Dim]-config_g.cases)
            else: Range = range(im.size[Dim]-config_g.cases, 0,-1)
            dic_infos = Anims.infos(self, max(Range.start, Range.stop), 0, kwargs)
            for Frames in Range:
                start = time.perf_counter()
                if Sens == "D": FramesAff = Range.start - Frames
                else:FramesAff = Frames
                dic_infos['frame'] = Frames
                dic_infos['delai'] = config_g.delai
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        if Orientation == "H":
                            R, V, B = im.getpixel((x+Frames, y))
                        else:
                            R, V, B = im.getpixel((x, y+Frames))
                        color_temp = [R, V, B]
                        if Picto[-9:-8] == "N":
                            for c in Color.colors:
                                col_calc = color_temp[c] * ColorCible[c]
                                base_matrix[x][y][c] = 255 if col_calc > 255 else col_calc
                        else:
                            base_matrix[x][y] = color_temp
                        #base_matrix[x][y] = Color.new_color(R,V,B) * ColorCible
                ColorCible = Color.varie(ColorCible)
                self.affiche(base_matrix, dic_infos, config_g.delai*Vitesse, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix

class Kino(Images):
    def play(self, base_matrix, kwargs={}):
        chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(config_g.cases) + "/kino/"
        if os.path.isdir(chemin):
            ColorCible = Color.new_color()
            Kinos  = [file for file in os.listdir(chemin) if file.lower().endswith('gif')]
            Anim = Kinos[random.randint(0,len(Kinos)-1)]
            imgAnim = chemin + Anim
            Vitesse = int(Anim[-6:-4])/10
            im = Image.open(imgAnim)
            lg_anim = len(list(enumerate(self._iter_frames(im))))
            dic_infos = Anims.infos(self, lg_anim, 0, kwargs)
            for i, frame in list(enumerate(self._iter_frames(im))):
                start = time.perf_counter()
                dic_infos['frame'] = lg_anim-i
                dic_infos['delai'] = config_g.delai
                rgb_im = frame.convert('RGB')
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        R, V, B = rgb_im.getpixel((x, y))
                        color_temp = [R, V, B]
                        if Anim[-7:-6] == "N":
                            for c in Color.colors:
                                col_calc = color_temp[c] * ColorCible[c]
                                base_matrix[x][y][c] = 255 if col_calc > 255 else col_calc
                        else:
                            base_matrix[x][y] = color_temp
                ColorCible = Color.varie(ColorCible)
                self.affiche(base_matrix, dic_infos, config_g.delai*Vitesse, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix
