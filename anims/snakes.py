import time
import random

from .base import Anims, MIX, RGB
from color import Color
from config import config_g


class Snakes(Anims):
    def play_std(self, base_matrix, kwargs={}):
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 7)
        kw_qte = kwargs['Quantite'] = kwargs.get('Quantite', 1)
        kw_sens = kwargs['Sens'] = kwargs.get('Sens', 0) # 0: sens = tous les sens, 1: 1 seul sens pour tous les snakes
        if kw_qte < 1: kw_qte = 1
        quantite = range(0, kw_qte)
        ColorCible = []
        x = []
        y = []
        Direction = []
        Direction2 = []
        for idx in quantite:
            ColorCible.append(Color.new_color())
            x.append(random.randrange(0,config_g.cases))
            y.append(random.randrange(0,config_g.cases))
            Direction.append(random.randint(0,3))
            Direction2.append(random.randint(0,3))
            if idx > 0 and kw_sens == 1:
                Direction[idx] = Direction[0]
                Direction2[idx] = Direction2[0]
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            base_matrix.fade(kw_estompage)
            for idx in quantite:
                Direction2[idx] = random.randint(0,3)
                if idx > 0 and kw_sens == 1:
                    Direction2[idx] = Direction2[0]
                if Direction2[idx] != (Direction[idx] - 2) % 4:
                    # Pour empecher le snake de faire demi-tour
                    Direction[idx] = Direction2[idx]
                if Direction[idx] == 0:
                    y[idx] -= 1
                elif Direction[idx] == 1:
                    x[idx] += 1
                elif Direction[idx] == 2:
                    y[idx] += 1
                else:
                    x[idx] -= 1
                for c in Color.colors:
                    base_matrix[x[idx]%config_g.cases][y[idx]%config_g.cases][c] = base_matrix[x[idx]%config_g.cases][y[idx]%config_g.cases][c] + ColorCible[idx][c]
                ColorCible[idx] = Color.varie(ColorCible[idx])
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix
    def play_rgb(self, base_matrix, kwargs={}):
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 7)
        kw_sens = kwargs['Sens'] = kwargs.get('Sens', 0) # 0: sens = tous les sens, 1: 1 seul sens pour tous les snakes
        
        ColorCible = Color.new_color()
        x = []
        y = []
        Direction = []
        Direction2 = []
        for color in Color.colors:
            x.append(random.randrange(0,config_g.cases))
            y.append(random.randrange(0,config_g.cases))
            Direction.append(random.randint(0,3))
            Direction2.append(random.randint(0,3))
            if color > 0 and kw_sens == 1:
                Direction[color] = Direction[0]
                Direction2[color] = Direction2[0]
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            base_matrix.fade(kw_estompage)
            for color in Color.colors:
                Direction2[color] = random.randint(0,3)
                if color > 0 and kw_sens == 1:
                    Direction2[color] = Direction2[0]
                if Direction2[color] != (Direction[color] - 2) % 4:
                    # Pour empecher le snake de faire demi-tour
                    Direction[color] = Direction2[color]
                if Direction[color] == 0:
                    y[color] -= 1
                elif Direction[color] == 1:
                    x[color] += 1
                elif Direction[color] == 2:
                    y[color] += 1
                else:
                    x[color] -= 1
                base_matrix[x[color]%config_g.cases][y[color]%config_g.cases][color] = base_matrix[x[color]%config_g.cases][y[color]%config_g.cases][color] + ColorCible[color]
            ColorCible = Color.varie(ColorCible)
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix

class Snake(Snakes):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Snakes()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Snake_RGB(Snakes):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Snakes()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Snake_RGB_OneD(Snakes):# ça marche ça ?????????
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Sens'] = 1
        obj = Snakes()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Snakes_Multi(Snakes):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Quantite'] = kwargs.get('Quantite', random.randint(2,4))
        obj = Snakes()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Snakes_Multi_OneD(Snakes):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Quantite'] = kwargs.get('Quantite', random.randint(2,4))
        kwargs['Sens'] = 1
        obj = Snakes()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix