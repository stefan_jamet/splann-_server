import time
#import copy
import random
from math import *

import os

from PIL import Image # installer pillow

from .base import Anims, TOUT, MIX, RGB
from .images import Images
from color import Color
from config import config_g

class Test(Images):
    coeff = 0
    _test = True
    def play(self, base_matrix, kwargs={}):
        chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(config_g.cases) + "/TestLEDs.gif"
        if os.path.isfile(chemin):
            im = Image.open(chemin)
            lg_anim = len(list(enumerate(self._iter_frames(im))))
            dic_infos = Anims.infos(self, lg_anim, 0, kwargs)
            for i, frame in list(enumerate(self._iter_frames(im))):
                dic_infos['frame'] = lg_anim-i
                dic_infos['delai'] = config_g.delai
                rgb_im = frame.convert('RGB')
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        R, V, B = rgb_im.getpixel((x, y))
                        base_matrix[x][y] = Color.new_color(R,V,B)
                self.affiche(base_matrix, dic_infos, config_g.delai*2)
                config_g.pause = True
                if config_g.end or config_g.zap:
                    config_g.pause = False
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        config_g.pause = False
        return base_matrix

class Flash(Anims):
    def play(self, base_matrix, kwargs={}):
        # Valeurs par defaut
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(75,150))
        kwargs['Quantite'] = kwargs.get('Quantite', random.randint(int(config_g.cases/5),int(config_g.cases/2)))
        quantite = range(0, kwargs['Quantite'])
        ColorCible = Color.new_color()
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            for x in config_g.cases_r:
                for y in config_g.cases_r:
                    base_matrix[x][y] = Color.new_color(0,0,0)
            for q in quantite:
                base_matrix[random.randrange(0, config_g.cases)][random.randrange(0, config_g.cases)] = ColorCible

            ColorCible = Color.new_color()
            self.affiche(base_matrix, dic_infos, config_g.delai/4, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix