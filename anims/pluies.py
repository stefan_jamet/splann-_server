import time
import copy
import random

from .base import Anims, TOUT, MIX, RGB
from color import Color
from config import config_g


class Pluies(Anims):
    def play_std(self, base_matrix, kwargs={}):
        # Valeurs par defaut
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 10)
        kw_sym = kwargs['Symetrie'] = kwargs.get('Symetrie', 0)

        ColorCible = Color.new_color()
        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            base_matrix.fade(kw_estompage)
            x = random.randrange(int(0-100*(1/(10+config_g.cases*config_g.cases))), config_g.cases)
            y = random.randrange(int(0-100*(1/(10+config_g.cases*config_g.cases))), config_g.cases)
            if x >= 0 and y >= 0:
                base_matrix[x][y] = copy.copy(ColorCible)
                if kw_sym == 1 or kw_sym == 4:
                    base_matrix[x][config_g.cases-1-y] = copy.copy(ColorCible)
                if kw_sym == 2 or kw_sym == 4:
                    base_matrix[config_g.cases-1-x][y] = copy.copy(ColorCible)
                if kw_sym == 3 or kw_sym == 4:
                    base_matrix[config_g.cases-1-x][config_g.cases-1-y] = copy.copy(ColorCible)
            ColorCible = Color.new_color()
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix
    def play_rgb(self, base_matrix, kwargs={}):
        # Valeurs par defaut
        kw_frames = kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 10)
        kw_sym = kwargs['Symetrie'] = kwargs.get('Symetrie', 0)

        dic_infos = Anims.infos(self, kw_frames, 0, kwargs)
        for Frames in range(0, kw_frames):
            start = time.perf_counter()
            dic_infos['frame'] = Frames
            dic_infos['delai'] = config_g.delai
            ColorCible = Color.new_color()
            base_matrix.fade(kw_estompage)
            for color in Color.colors:
                x = random.randrange(0,config_g.cases)
                y = random.randrange(0,config_g.cases)
                base_matrix[x][y][color] += copy.copy(ColorCible[color])
                if kw_sym == 1 or kw_sym == 4:
                    base_matrix[x][config_g.cases-1-y][color] = copy.copy(ColorCible[color])
                if kw_sym == 2 or kw_sym == 4:
                    base_matrix[config_g.cases-1-x][y][color] = copy.copy(ColorCible[color])
                if kw_sym == 3 or kw_sym == 4:
                    base_matrix[config_g.cases-1-x][config_g.cases-1-y][color] = copy.copy(ColorCible[color])
            ColorCible = Color.new_color()
            self.affiche(base_matrix, dic_infos, config_g.delai, start)
            if config_g.end or config_g.zap:
                return base_matrix
            if config_g.pause:
                while config_g.pause:
                    if config_g.end == True:
                        break
                    time.sleep(0.1)
        return base_matrix

class Pluie(Pluies):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Pluies()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Pluie_RGB(Pluies):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Pluies()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym(Pluies):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = 4
        obj = Pluies()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym2(Pluies):
    _couleur = MIX
    coeff = 15
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = random.randint(1,3)
        obj = Pluies()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym_RGB(Pluies):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = 4
        obj = Pluies()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym2_RGB(Pluies):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = random.randint(1,3)
        obj = Pluies()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Pluie_Empil(Pluies):
    _couleur = MIX
    coeff = 5
    def play(self, base_matrix, kwargs={}):
        kwargs['Estompage'] = kwargs.get('Estompage', config_g.cases*config_g.cases*config_g.cases)
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        obj = Pluies()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Pluie_RGB_Empil(Pluies):
    _couleur = MIX
    coeff = 1
    def play(self, base_matrix, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Estompage'] = kwargs.get('Estompage', config_g.cases*config_g.cases*config_g.cases)
        obj = Pluies()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym_Empil(Pluies):
    _couleur = MIX
    coeff = 5
    def play(self, base_matrix, kwargs={}):
        kwargs['Estompage'] = kwargs.get('Estompage', config_g.cases*config_g.cases*config_g.cases)
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = 4
        obj = Pluies()
        base_matrix = obj.play_std(base_matrix, kwargs)
        return base_matrix
class Pluie_Sym_RGB_Empil(Pluies):
    _couleur = MIX
    coeff = 1
    def play(self, base_matrix, kwargs={}):
        kwargs['Estompage'] = kwargs.get('Estompage', config_g.cases*config_g.cases*config_g.cases)
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
        kwargs['Symetrie'] = 4
        obj = Pluies()
        base_matrix = obj.play_rgb(base_matrix, kwargs)
        return base_matrix