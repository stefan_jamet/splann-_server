import time

from color import Color

from config import config_g
from logger import logger

TOUT = 0
MIX = 1
RGB = 2


class Anims(object):
    coeff = 10
    _audio = False
    _test = False
    _couleur = TOUT # Mix / RGB / Tout
    
    def play(self, base_matrix, kwargs={}):
        logger.warning('Prototype de la fonction Play. Vous ne devriez pas voir ça.')
        return base_matrix
    
    def infos(self, frames_tot, frame, kwargs={}):
        kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__))
        return {'frame':frame,
            'frames_total':frames_tot,
            'animation_name': kwargs['_nom'],
            'delai':config_g.delai,
            'hue_global': Color.hue_global,
            'delta_global':Color.delta_global,
            'variation_global':Color.variation_global}
    
    def affiche(self, matrice, infos, delai, start=0):
        if config_g._serial_ready:
            "Si la sortie série est activée, on envoi ce qu'il faut, où il faut"
            config_g._serial.flushInput()
            mesg = ','.join(str(int(color)) for x in matrice for y in x for color in y)
            config_g._serial.write(mesg.encode('ascii'))
        matrice.infos = infos
        stop = time.perf_counter()
        if start != 0:
            pause = delai/1000 - (stop-start)
        else:
            pause = delai/1000
        if pause > 0:
            time.sleep(pause)
        else:
            logger.warning(infos['animation_name'] + " f°" + str(infos['frame']) + ': ' + str(pause))