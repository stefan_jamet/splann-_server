import time
import random
from math import *

from .base import Anims, MIX, RGB
from color import Color
from config import config_g

#import numba

class Pulsations(Anims):
    pass

class Pulsation(Pulsations):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 5)
        
        ColorCible = Color.new_color()
        MaxDistance = 200/sqrt((config_g.cases / 2)**2 + (config_g.cases / 2)**2)
        Tableaux = random.randint(4,9)
        dic_infos = Anims.infos(self, Tableaux*25, 0, kwargs)
        range0_25 = range(0, 25)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for truc in range(0,Tableaux):
            ColorCible = Color.varie(ColorCible)
            ColorTemp = Color.new_color(0,0,0)
            CentreX = random.randrange(0, config_g.cases)
            CentreY = random.randrange(0, config_g.cases)
            for Xeq in range0_25:
                start = time.perf_counter()
                dic_infos['frame'] = truc*25+Xeq
                dic_infos['delai'] = config_g.delai
                PulsationX = int((245/(0.5*(Xeq-17)**2+1))+(150/(0.5*(Xeq-10)**2+1)))
                for color in Color.colors:
                    ColorTemp[color] = int(ColorCible[color] * PulsationX/255)
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        Distance = sqrt((CentreX-x)**2 + (CentreY-y)**2) * MaxDistance
                        for color in Color.colors:
                            var_t = base_matrix[x][y][color] - fade_val
                            base_matrix[x][y][color] = 0 if var_t < 0 else var_t
                            var_t2 = base_matrix[x][y][color] + ColorTemp[color] - Distance
                            base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix
class Pulsation_RGB(Pulsations):
    _couleur = RGB
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 5)
        kw_frames = kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
        
        ColorCible = Color.new_color()
        MaxDistance = 200/sqrt((config_g.cases / 2)**2 + (config_g.cases / 2)**2)
        dic_infos = Anims.infos(self, kw_frames*25, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for Tableaux in range(0, kw_frames):
            ColorCible = Color.varie(ColorCible)
            ColorTemp = Color.new_color(0,0,0)
            CentreX = [random.randrange(0, config_g.cases),random.randrange(0, config_g.cases),random.randrange(0, config_g.cases)]
            CentreY = [random.randrange(0, config_g.cases),random.randrange(0, config_g.cases),random.randrange(0, config_g.cases)]
            for Xeq in range(0,25):
                start = time.perf_counter()
                dic_infos['frame'] = Tableaux*25+Xeq
                dic_infos['delai'] = config_g.delai
                PulsationX = int((245/(0.5*(Xeq-17)**2+1))+(150/(0.5*(Xeq-10)**2+1)))
                for color in Color.colors:
                    ColorTemp[color] = int(ColorCible[color] * PulsationX/255)
                for x in range(0,config_g.cases):
                    for y in range(0,config_g.cases):
                        for color in Color.colors:
                            var_t = base_matrix[x][y][color] - fade_val
                            base_matrix[x][y][color] = 0 if var_t < 0 else var_t
                            Distance = sqrt((CentreX[color]-x)**2 + (CentreY[color]-y)**2) * MaxDistance
                            var_t2 = base_matrix[x][y][color] + (ColorTemp[color] - Distance)
                            base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix


class Cercles(Pulsations):
    _couleur = MIX
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 5)
        kw_frames = kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
        dic_infos = Anims.infos(self, kw_frames*25, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for Tableaux in range(0, kw_frames):
            ColorCible = Color.new_color()
            CentreX = random.randrange(0, config_g.cases)
            CentreY = random.randrange(0, config_g.cases)
            for Frames in range(0, config_g.cases*5*2):
                start = time.perf_counter()
                dic_infos['frame'] = Tableaux*25+Frames
                dic_infos['delai'] = config_g.delai
                Rayon = Frames/5
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        for color in Color.colors:
                            base_matrix[x][y][color] -= fade_val
                        RayonCalc = sqrt((x-CentreX)**2+(y-CentreY)**2)
                        if Rayon > RayonCalc-0.6 and Rayon < RayonCalc+0.6:
                            for color in Color.colors:
                                var_t2 = ColorCible[color]*(1-(RayonCalc-Rayon))
                                base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                        if Rayon-config_g.cases/1.5 > RayonCalc-0.5 and Rayon-config_g.cases/1.5 < RayonCalc+0.5:
                            for color in Color.colors:
                                var_t2 = ColorCible[color]*(1-(RayonCalc-Rayon+config_g.cases/1.5))
                                base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix

class Cercles_RGB(Pulsations):
    _couleur = RGB
    
    #@numba.autojit
    def play(self, base_matrix, kwargs={}):
        kw_estompage = kwargs['Estompage'] = kwargs.get('Estompage', 5)
        kw_frames = kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
        dic_infos = Anims.infos(self, kw_frames*25, 0, kwargs)
        fade_val = 255/(kw_estompage*config_g.cases/5)
        for Tableaux in range(0, kw_frames):
            ColorCible = Color.new_color()
            CentreX = [random.randrange(0, config_g.cases),random.randrange(0, config_g.cases),random.randrange(0, config_g.cases)]
            CentreY = [random.randrange(0, config_g.cases),random.randrange(0, config_g.cases),random.randrange(0, config_g.cases)]
            RayonCalc = [0,0,0]
            for Frames in range(0, config_g.cases*5*2):
                start = time.perf_counter()
                dic_infos['frame'] = Tableaux*25+Frames
                dic_infos['delai'] = config_g.delai
                Rayon = Frames/5
                for x in config_g.cases_r:
                    for y in config_g.cases_r:
                        for color in Color.colors:
                            base_matrix[x][y][color] -= fade_val
                            RayonCalc[color] = sqrt((x-CentreX[color])**2+(y-CentreY[color])**2)
                            if Rayon > RayonCalc[color]-0.6 and Rayon < RayonCalc[color]+0.6:
                                var_t2 = ColorCible[color]*(1-(RayonCalc[color]-Rayon))
                                base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                            if Rayon-config_g.cases/1.5 > RayonCalc[color]-0.5 and Rayon-config_g.cases/1.5 < RayonCalc[color]+0.5:
                                var_t2 = ColorCible[color]*(1-(RayonCalc[color]-Rayon+config_g.cases/1.5))
                                base_matrix[x][y][color] = 0 if var_t2 < 0 else 255 if var_t2 > 255 else var_t2
                self.affiche(base_matrix, dic_infos, config_g.delai/2, start)
                if config_g.end or config_g.zap:
                    return base_matrix
                if config_g.pause:
                    while config_g.pause:
                        if config_g.end == True:
                            break
                        time.sleep(0.1)
        return base_matrix
