from config import config_g
from color import Color


class Matrix(list):
    """
    Classe pour gérer les matrices
    """
    
    def __init__(self, *args):
        """
        Initialisation des matrices, transfert ses arguments tels quel à la classe Color
        """
        self.extend([[Color.new_color(*args) for i in config_g.cases_r] for j in config_g.cases_r])
        self.infos = {}
    
    def fade(self, coeff = 6):
        """
        le coeff est grosso-modo le nombre de fades a appliquer pour éteindre une LED qui était au max
        """
        if coeff != 0:
            fade_val = 255/(coeff*config_g.cases/5)
            self[:] = [[[0 if c - fade_val < 0 else c - fade_val for c in y] for y in x] for x in self]
