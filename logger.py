import logging
 
# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
 
# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s', "%H:%M:%S")

# création d'un handler qui va rediriger chaque écriture de log
# sur la console
steam_handler = logging.StreamHandler()
steam_handler.setLevel(logging.DEBUG)
steam_handler.setFormatter(formatter)
logger.addHandler(steam_handler)


logger.ERROR = logging.ERROR
logger.WARNING = logging.WARNING
logger.INFO = logging.INFO
logger.DEBUG = logging.DEBUG