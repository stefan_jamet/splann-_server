#! /usr/bin/env python

import argparse
import threading

import time

from logger import logger
from config import config_g
from matrix import Matrix
from playlist import Playlist
from color import Color
from wamp_server import WAMP_Server



VERSION = "0.1.0"

class Splann3():
    
    def __init__(self, cases, delai, port, baudrate, serial_output):
        logger.info("Initialisation du serveur")
        logger.info("Cases : " + str(cases))
        logger.info("Delai : " + str(delai) + "ms")
        logger.info("Port série : " + str(port))
        logger.info("Baudrate : " + str(baudrate))
        
        config_g.cases = cases
        config_g.delai = delai
        
        config_g.port = port
        config_g.baudrate = baudrate
        config_g.serial_output = serial_output
        
        self.matrix = Matrix(0,0,0)
        
        self.playlist = Playlist()
        
        thread_WAMP = threading.Thread(target=self.t_WAMP, daemon=True)
        thread_WAMP.start()
        
        logger.debug("Serveur initialisé")

    
    def t_WAMP(self):
        logger.debug("lancement du thread WAMP")
        wamp_s = WAMP_Server()
        wamp_s.run(self)
    
    def t_animations(self):
        while not config_g.end:
            config_g.zap = False
            config_g.draw = False
            next_anim = self.playlist.next_anim()
            anim = next_anim[0]()
            arguments = next_anim[1]
            logger.info("# " + str(next_anim))
            self.matrix = anim.play(base_matrix=self.matrix, kwargs=arguments)

    
    def run(self):
        logger.debug("Lancement du serveur")
        
        thread_animations = threading.Thread(target=self.t_animations, daemon=True)
        thread_animations.start()
        
        while not config_g.end:
            if not config_g.pause:
                Color.hue_global = (Color.hue_global + Color.variation_global / 10000)%1
            time.sleep(config_g.delai/1000)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Lance le serveur Splann³")
    parser.add_argument('-c', "--cases", action="store", type=int, help="Nombre de cases", default=5, dest="CASES", metavar="X")
    parser.add_argument('-d', "--delai", action="store", type=int, help="Delai d'affichage des frames (en ms)",  default=200, dest="DELAI", metavar="XXX")
    parser.add_argument('-p', "--port", action="store", help="Port série à utiliser. /dev/null pour désactiver la sortie série", default="/dev/ttyACM0", dest="SERIAL_PORT", metavar="PORT")
    parser.add_argument('-b', "--baudrate", action="store", help="Baudrate pour la liason série", default=115200, metavar="XX", dest="BAUDRATE")
    parser.add_argument('-v', '--verbose', action="count", help="Mode bavard")
    parser.add_argument('--version', action="version", help="Affiche la version du programme", version='%(prog)s '+ VERSION)
    
    args = parser.parse_args()
    
    # Définition du degré de verbosité
    if args.verbose == None:
        logger.setLevel(logger.ERROR)
    elif args.verbose == 1:
        logger.setLevel(logger.WARNING)
    elif args.verbose == 2:
        logger.setLevel(logger.INFO)
    elif args.verbose > 2:
        logger.setLevel(logger.DEBUG)
    
    # Active ou désactive la sortie série suivant le port entré
    args_serial_output = True
    if args.SERIAL_PORT[:5] != "/dev/" or args.SERIAL_PORT in ["/dev/null", "/dev/none"]:
        #si ça ne commence pas par '/dev/', c'est probablement foireux
        args_serial_output = False
        args.SERIAL_PORT = '/dev/null'
    logger.info("Démarrage du serveur")
    
    splann3 = Splann3(cases=args.CASES, delai=args.DELAI, port=args.SERIAL_PORT, baudrate=args.BAUDRATE, serial_output=args_serial_output)
    splann3.run()