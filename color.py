import colorsys
import random
import math

R = 0
G = 1
B = 2

class Color(list):
    """
    Classe pour gérer les couleurs
    Les attributs de classe gèrent la couleur générale demandée par le programme
    
    
    Initialisation :
        Color.new_color() : donne une couleur au hasard (dans les limites générales)
        Color.new_color(r,v,b) ou Color.new_color(teinte): donne la couleur spécifiée

    Cercle chromatique :
        Rouge:		0°		0.0
        Orange:		30°		0.125
        Jaune:		60°		0.25
        Vert:		120°	0.375
        Cyan:		180°	0.5
        Bleu:		240°	0.625
        Violet:		270°	0.75
        Magenta:	300°	0.875
        Rouge:		360°	1.0
    """
    
    hue_global = random.random() # met la teinte générale quelque part sur le cercle chromatique, entre 0 et 1
    delta_global = 0.5 # autorise tout le cercle (+-0.5 = 1)
    variation_global = random.randint(-30,30) # on decale la teinte de pas grand chose a chaque fois
    colors = range(0,3)
    
    @classmethod
    def rgb_2_hsv(cls, Cible):
        ret_val = colorsys.rgb_to_hsv(Cible[R]/255, Cible[G]/255, Cible[B]/255)
        return [ret_val[0], ret_val[1], ret_val[2]]
    
    @classmethod
    def hsv_2_rgb(cls, Cible):
        r, g, b = colorsys.hsv_to_rgb(Cible[0], Cible[1], Cible[2])
        return [int(r*255), int(g*255), int(b*255)]
    
    @classmethod
    def new_color(cls, *args):
        if len(args) == 3:
            return [cls.rgb_min_max(args[R]), cls.rgb_min_max(args[G]), cls.rgb_min_max(args[B])]
        elif len(args) == 1:
            r, g, b = colorsys.hsv_to_rgb(cls.hue_min_max(args[0]), 1, 1)
            return [cls.rgb_min_max(r*255), cls.rgb_min_max(g*255), cls.rgb_min_max(b*255)]
        else:
            return cls.color_rand()

    @classmethod
    def color_rand(cls):
        h = cls.hue_min_max(random.uniform(Color.hue_global - Color.delta_global, Color.hue_global + Color.delta_global))
        s = cls.hsv_min_max(random.uniform(7/8 + Color.delta_global/4, 1))
        v = cls.hsv_min_max(random.uniform(7/8 + Color.delta_global/4, 1))
        r, g, b = colorsys.hsv_to_rgb(h, s, v)
        return [cls.rgb_min_max(r*255), cls.rgb_min_max(g*255), cls.rgb_min_max(b*255)]
    
    @classmethod
    def varie(cls, Source, Cible = None):# Pas sur que ça marche ça...
        ' Fait varier la couleur suivant la variation_teinte_generale, ou tend vers la couelur spécifiée'
        Pas = 1/100
        S_t = cls.rgb_2_hsv(Source)
        S_h, S_s, S_v = S_t[0], S_t[1], S_t[2]
        if type(Cible) == list:#TODO ça c'est pas mal long
            C_t = cls.rgb_2_hsv(Cible)
            C_h, C_s, C_v = C_t[0], C_t[1], C_t[2]
            if Source == Cible:
                # Si la couleur est atteinte, on ne fait rien
                pass
            else:

                if S_s == 0 or C_s == 0:
                # on va ou on vient d'un gris, du coups le gris prend la teinte de l'autre
                    if S_h < C_h:
                        S_h = C_h
                    else:
                        C_h = S_h
                if S_h != C_h:
                    if (S_h + Pas) > C_h and (S_h - Pas) < C_h: S_h = C_h
                    elif abs(S_h - C_h) <= 0.5:
                        if S_h < C_h:
                            S_h += Pas
                        else:
                            S_h -= Pas
                    else:
                        if S_h < C_h:
                            S_h -= Pas
                        else:
                            S_h += Pas
                    S_h = cls.hue_min_max(S_h)
                if S_s != C_s:
                    if (S_s + Pas) > C_s and (S_s - Pas) < C_s:
                        S_s = C_s
                    elif S_s > C_s:
                        S_s -= Pas
                    else:
                        S_s += Pas
                    S_s = cls.hsv_min_max(S_s)
                if S_v != C_v:
                    if (S_v + Pas) > C_v and (S_v - Pas) < C_v:
                        S_v = C_v
                    elif S_v > C_v:
                        S_v -= Pas
                    else:
                        S_v +=Pas
                    S_v = cls.hsv_min_max(S_v)
        else:# on évolue de la même manière que la teinte générale
            S_h = cls.hue_min_max(S_h + Color.variation_global/10000)
        return cls.hsv_2_rgb((S_h, S_s, S_v))
    
    @staticmethod
    def rgb_min_max(x):
        return  0 if x < 0 else 255 if x > 255 else int(x)
    
    @staticmethod
    def hue_min_max(x):
        x = x - math.floor(x)
        if x < 0:
            x += 1
        elif x > 1:
            x -= 1
        return float(x)
    
    @staticmethod
    def hsv_min_max(x):
        return 0 if x < 0 else 1 if x > 1 else float(x)
