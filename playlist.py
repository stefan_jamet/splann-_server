import random

from anims import *

from config import config_g
from logger import logger


base_class_anim = vars()['Anims']

TOUT = 0
MIX = 1
RGB = 2

class Playlist(list):
    def __init__(self):
        self._animations = []
        self._animations_coeff = []
        self.current = tuple
        
        # filtres pour les animations
        self._test = False
        self._audio = False
        self._couleur = TOUT
        
        self._make_lists()
    
    def random_anim(self):
        self._make_lists()
        return self.animations_coeff[random.randint(0,len(self.animations_coeff)-1)] #TODO lent
    
    def add(self, anim, args={}, position=-1):
        logger.debug("-ADD- " + anim)
        if any(anim == str(elt).split("'")[1] for elt in self.animations):
            for element in self.animations:
                if anim == str(element).split("'")[1]:
                    elt = element
                    anim_2_add = self.animations[self.animations.index(elt)]
                    if position == -1:
                        self.append((anim_2_add, args))
                    else:
                        self.insert(position, (anim_2_add, args))
                    break
    
    def next_anim(self):
        if config_g.stick:
            return (self.current[0], {})
        elif len(self) == 0:
            self.current = (self.random_anim(), {}) #TODO pas mal lent aussi ça...
        else:
            self.current = self[0]
            self.pop(0)
        return self.current
    
    @property
    def animations(self):
        self._make_lists()
        return self._animations

    @property
    def animations_coeff(self):
        self._make_lists()
        return self._animations_coeff

    @property
    def filter_test(self):
        return self._test
    @filter_test.setter
    def filter_test(self, value):
        self._test = value
        self._make_lists()

    @property
    def filter_audio(self):
        return self._audio
    @filter_audio.setter
    def filter_audio(self, value):
        self._audio = value
        self._make_lists()

    @property
    def filter_color(self):
        return self._couleur
    @filter_color.setter
    def filter_color(self, value):
        self._couleur = value
        self._make_lists()
    
    def _make_lists(self):
        """ Génère la liste des animations disponibles, en fonction de leur coefficient """
        # le [:] = [], c'est pour vider la liste, et repartir sur une base propre. Je crois.
        self._animations[:] = []
        self._animations.extend(self._make_recursive_list(base_class_anim, False))
        self._animations_coeff[:] = []
        self._animations_coeff.extend(self._make_recursive_list(base_class_anim, True))
    
    def _make_recursive_list(self, base_class, coeff):
        anims = []
        for subclass in base_class.__subclasses__():
            if self._test_subclass(subclass):
                if subclass.__subclasses__() != []:
                    anims.extend(self._make_recursive_list(subclass, coeff))
                else:
                    if coeff:
                        for i in range(0, subclass.coeff):
                            anims.append(subclass)
                    else:
                        anims.append(subclass)
        return anims
    
    def _test_subclass(self, subclass):
        """ Vérifie si on doit afficher cette animation, en fonction des flags définis """
        if subclass._test and not self.filter_test:
            return False
        if subclass._audio and not self.filter_audio:
            return False
        if self.filter_color != TOUT and subclass._couleur != TOUT and subclass._couleur != self.filter_color:
            return False
        return True